package API;

import java.io.BufferedReader;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import VOS.VOUsuarioPelicula;

import com.csvreader.CsvReader;

import VOS.VOFranquicia;
import VOS.VOGeneroPelicula;
import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOProgramacion;
import VOS.VOTeatro;
import VOS.VOUbicacion;
import VOS.VOUsuario;
import data_structures.Edge;
import data_structures.EncadenamientoSeparadoTH;
import data_structures.ListaEncadenada;
import data_structures.ListaEnlazada;
import data_structures.ListaEnlazadaSimple;
import data_structures.MaxHeapCP;
import data_structures.Nodo;
import data_structures.NodoCamino;
import data_structures.WeightedGraph;
import VOS.VORating;
import VOS.VORecorrido;
import VOS.VORed;

public class SistemaRecomendacion implements ISistemaRecomendacion {
	VOTeatro[] teatros;
	VOPelicula[] peliculas;
	VOProgramacion[] programacion;
	VORed[] red;

	//Generos
	String generos = "Adventure,Animation,Children,Comedy,Fantasy,Romance,Drama,Action,Crime,Thriller,Horror,Mystery,Sci-Fi,Documentary,IMAX,War,Musical,Western,Film-Noir,(no genres listed)";
	//-----------------------------------------
	//Estructuras
	//-----------------------------------------

	//Grafo con la informacion de los teatros
	WeightedGraph< String, VOTeatro> grafoteatros = new WeightedGraph<>(101);


	//Tabla hash con la ingormacion de las peliculas (llave = id , valor = VOPelicula)
	EncadenamientoSeparadoTH<Integer, VOPelicula> THPeliculas = new EncadenamientoSeparadoTH<>(97);

	// Tabla hash con la programacion por dia (llave = dia de que se quiera ; valor = arreglo de VOProgramacion con la informacion)
	EncadenamientoSeparadoTH<Integer, VOProgramacion[]> THProgramacion = new EncadenamientoSeparadoTH<>(11);

	//Tabla hash (llave = moviedID ; valor = tabla hash (llave = movieID ; similitud))
	EncadenamientoSeparadoTH<Integer, EncadenamientoSeparadoTH<Integer, Double>> THSimilitudes = new EncadenamientoSeparadoTH<Integer, EncadenamientoSeparadoTH<Integer, Double>>(101);

	//Tabla hash (llave = idUser valor = VOUsuaruio)
	EncadenamientoSeparadoTH< Long, VOUsuario> THUsuarios = new EncadenamientoSeparadoTH<>(101);

	//Tabla hash de generos (llave = String "nombre genero" ; valor = VOGEenero)
	EncadenamientoSeparadoTH<String, VOGeneroPelicula> THGeneros = new EncadenamientoSeparadoTH<>(13);
	
	//Tabla hash de teatros (llave = String "nombre teatro" ; valor = VOTeatro)
	EncadenamientoSeparadoTH<String, VOTeatro> THTeatros = new EncadenamientoSeparadoTH<>(100);
	private Time ultimaFuncion;
	private WeightedGraph <VOTeatro, VOTeatro> grafo;
	private Time [][] franjasHorarias;

	public SistemaRecomendacion (){
		grafo =  new WeightedGraph <VOTeatro, VOTeatro> (100); 
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		String [][] cadenasTiempos = {{"06:00","11:59"}, {"12:00","17:59"}, {"18:00", "23:59"}}; 
		franjasHorarias = new Time [3][2];
		for (int i = 0; i < franjasHorarias.length; i++){
			for (int j = 0; j < franjasHorarias[i].length; j++){
				try {
					franjasHorarias[i][j]= new Time (format.parse(cadenasTiempos[i][j]).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
	}
	@Override
	public ISistemaRecomendacion crearSR() {
		return new SistemaRecomendacion();
	}

	public boolean cargarTeatros(String ruta) {
		try{
			Gson g = new Gson();
			teatros = null;
			BufferedReader br = new BufferedReader(new FileReader(ruta));
			teatros = g.fromJson(br, VOTeatro[].class);
			br.close();
			for(VOTeatro t : teatros){
				t.parse();
				grafoteatros.agregarVertice(t.getNombre(), t);
				THTeatros.insertar(t.getNombre(), t);
			}
			
		}catch(Exception e ){
			e.printStackTrace();
			return false;
		}
		String[] g = generos.split(",");
		for(String s : g){
			VOGeneroPelicula nuevo = new VOGeneroPelicula();
			nuevo.setNombre(s);
			THGeneros.insertar(s, nuevo);
		}
		return true;
	}
	public boolean cargarPeliculas(String ruta){
		try {
			Gson g = new Gson();
			peliculas = null;
			BufferedReader br = new BufferedReader(new FileReader(ruta));
			peliculas = g.fromJson(br, VOPelicula[].class);
			br.close();
			for(VOPelicula p : peliculas){
				p.parse();
				THPeliculas.insertar(p.getId(), p);
			}
			System.out.println("Se Cargaron correctamente las peliculas");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean cargarCartelera(String ruta) {
		try {
			Gson g = new Gson();

			for (int i = 1; i < 6; i++) {

				String rutaV = "./data/programacion/dia"+i+".json";

				programacion = null;
				BufferedReader br = new BufferedReader(new FileReader(rutaV));
				programacion = g.fromJson(br, VOProgramacion[].class);
				THProgramacion.insertar(i, programacion);
			}
			completarInformacion();

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	
	public boolean cargarRed(String ruta) {
		try {
			Gson g = new Gson();
			red = null;
			BufferedReader br = new BufferedReader(new FileReader(ruta));
			red = g.fromJson(br, VORed[].class);
			br.close();

			for(VORed r : red){
				r.parse();
				grafoteatros.agregarArco(r.teatroString, r.teatro2String, r.tiempoMin);
				grafoteatros.agregarArco(r.teatro2String, r.teatroString, r.tiempoMin);
			}
			System.out.println("Se cargó correctamente el grafo con los pesos, id origen, id destino");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	private boolean cargarSimilitudes(){
		try {
			String fruta= "./data/simsMatriz.json";
			BufferedReader br = new BufferedReader(	new FileReader(fruta));
			JsonParser parser = new JsonParser();
			JsonElement element = parser.parse(br);
			JsonArray obj = element.getAsJsonArray();
			for(JsonElement elmento : obj){

				EncadenamientoSeparadoTH<Integer, Double> HTActual= new EncadenamientoSeparadoTH(19);
				Integer idPeliSim =new Integer(0) ;
				Integer idPelitemp =new Integer(0) ;

				JsonObject obj2 =elmento.getAsJsonObject();
				Set<Map.Entry<String, JsonElement>> entries = obj2.entrySet();
				for (Map.Entry<String, JsonElement> entry: entries) {
					if(entry.getKey().equals("movieId")){
						idPelitemp =Integer.parseInt(entry.getValue().getAsString());
						THSimilitudes.insertar(idPelitemp, HTActual);
						break;
					}
					else {
						idPelitemp = Integer.parseInt(entry.getKey());
						Double sim;
						if (!entry.getValue().getAsString().equals("NaN")) sim =Double.parseDouble(entry.getValue().getAsString());
						else sim =0.0;
						HTActual.insertar(idPelitemp, sim);						 
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	private boolean cargarUsuarios(){
		String ruta = "./data/newRatings.csv";
		try(BufferedReader br = new BufferedReader(new FileReader(ruta));) {
			String linea = br.readLine();
			while (linea!=null) {
				if (linea.startsWith("u")){linea = br.readLine();}
				
				
				Long idusertemp, idbefore = 0L;
				
				String[] datos = linea.split(",");
				
				idusertemp = Long.parseLong(datos[0]);
				
				if( idbefore != idusertemp){
					VOUsuario nuevo = new VOUsuario();
					nuevo.setId(idusertemp);
					THUsuarios.insertar(idusertemp, nuevo);
					idbefore = idusertemp;
				}
				linea=br.readLine();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}




	public int sizeMovies() {
		return peliculas.length;
	}


	public int sizeTeatros() {
		return teatros.length;
	}


	private void completarInformacion(){
		for (int i = 1; i < 6; i++) {
			VOProgramacion[] pro = THProgramacion.darValor(i);
			for (int j = 0; j < pro.length; j++) {
				VOProgramacion a = pro[j];
				for (int k = 0; k < teatros.length; k++)
					if(a.nombreTeatro.equals(teatros[k].getNombre()))
						teatros[k].getProgramacionDias().insertar(i, a);
			}
		}
	}
	public ListaEnlazadaSimple<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha)  {
		ListaEnlazadaSimple<VOPeliculaPlan> listaRetorno = new ListaEnlazadaSimple<>();
		VOProgramacion[] peliculasDia = THProgramacion.darValor(fecha);
		
		//Grafos de cada franja
		WeightedGraph<String, VOTeatro> franja1 = new WeightedGraph<>(13);
		WeightedGraph<String, VOTeatro> franja2 = new WeightedGraph<>(13);
		WeightedGraph<String, VOTeatro> franja3 = new WeightedGraph<>(13);
		
		try {
			
			filtroPorfranja(fecha, 1);
			for(VOTeatro t : teatros)
				franja1.agregarVertice(t.getNombre(), t);
			
			filtroPorfranja(fecha, 2);
			for(VOTeatro t : teatros)
				franja2.agregarVertice(t.getNombre(), t);
			
			filtroPorfranja(fecha, 3);
			
			for(VOTeatro t : teatros)
				franja3.agregarVertice(t.getNombre(), t);
			
			for(VORed r : red){
				franja1.agregarArco(r.teatroString, r.teatro2String, r.tiempoMin);
			}
			
			for(VORed r : red){
				franja2.agregarArco(r.teatroString, r.teatro2String, (r.tiempoMin*1.15));
			}
			for(VORed r : red){
				franja3.agregarArco(r.teatroString, r.teatro2String, (r.tiempoMin*1.2));
			}
			
		
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return listaRetorno;
	}
	/**
	 * 
	 * @param f: 1 franja mañana 2 franja tarde 3 franja noche
	 * @return lista de tetros con el filtro
	 * @throws ParseException 
	 */
	private void filtroPorfranja(int fecha , int f) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat("HH:mm a");
		for(VOTeatro t : teatros){
			if(f==1){
				
				VOProgramacion pro = t.getProgramacionDias().darValor(fecha);
				
				for (int i = 0;  pro != null && i < pro.listapeliculas.movies.length; i++) {
					for (int j = 0; j < pro.listapeliculas.movies[i].listaHoras.length; j++) {
						String hora = pro.listapeliculas.movies[i].listaHoras[j].hora;
						hora = hora.replace('a', 'A');
						hora = hora.replace('p', 'P');
						hora = hora.substring(0,hora.indexOf('.'))+"M";
						if(hora.contains("P"))
							continue;
						else{
							Date horaa = format.parse(hora);
							int horadeldia = horaa.getHours();
							if(!(horadeldia < 12) && !(horadeldia >= 8)){
								pro.listapeliculas.movies[i].listaHoras[j].hora = "00:00 PM";
							}
						}
						
					}
				}
			}
			if(f==2){
				ListaEnlazadaSimple<VOTeatro> retorno= new ListaEnlazadaSimple<>();
				VOProgramacion pro = t.getProgramacionDias().darValor(fecha);
				for (int i = 0; pro != null && i < pro.listapeliculas.movies.length; i++) {
					for (int j = 0; j < pro.listapeliculas.movies[i].listaHoras.length; j++) {
						String hora = pro.listapeliculas.movies[i].listaHoras[j].hora;
						hora = hora.replace('a', 'A');
						hora = hora.replace('p', 'P');
						hora = hora.substring(0,hora.indexOf('.'))+"M";
						if(hora.contains("P")){
							continue;
						}else{
							Date horaa = format.parse(hora);
							int horadeldia = horaa.getHours();
							if(!(horadeldia > 6) && !(horadeldia <= 12))
								pro.listapeliculas.movies[i].listaHoras[j].hora = "00:00 PM";
						}
					}	
				}
				
			}
			if(f==3){
				ListaEnlazadaSimple<VOTeatro> retorno= new ListaEnlazadaSimple<>();
				VOProgramacion pro = t.getProgramacionDias().darValor(fecha);
				for (int i = 0; pro != null && i < pro.listapeliculas.movies.length; i++) {
					for (int j = 0; j < pro.listapeliculas.movies[i].listaHoras.length; j++) {
						String hora = pro.listapeliculas.movies[i].listaHoras[j].hora;
						hora = hora.replace('a', 'A');
						hora = hora.replace('p', 'P');
						hora = hora.substring(0,hora.indexOf('.'))+"M";
						if(hora.contains("P")){
							continue;
						}else{
							Date horaa = format.parse(hora);
							int horadeldia = horaa.getHours();
							if(!(horadeldia >= 6) && !(horadeldia < 12)){
								pro.listapeliculas.movies[i].listaHoras[j].hora = "00:00 PM";
							}
						}
					}	
				}
				
			}
			if(f>3)break;
		}
		
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero, VOUsuario usuario) {
		// TODO Auto-generated method stub
		//Declaro mi lista para retornar
		ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
		//Declaracion de las listas, cada lista tiene un plan de pelicula por dia
		ListaEnlazadaSimple<VOPeliculaPlan> dia1 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia2 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia3 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia4 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> dia5 = new ListaEnlazadaSimple<VOPeliculaPlan>();
		//A cada lista le asigno el plan de pelicula por dia
		dia1 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 1);
		dia2 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 2);
		dia3 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 3);
		dia4 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 4);
		dia5 = (ListaEnlazadaSimple<VOPeliculaPlan>) PlanPeliculas(usuario, 5);
		//Recorro dia 1 y miro sus generos
		ListaEnlazadaSimple<VOGeneroPelicula> generos1 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia1.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero1 = new VOGeneroPelicula();
			for(int j = 0; j<dia1.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero1.setNombre(dia1.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos1.agregarElementoFinal(genero1);
		}
		//Ahora recorro mi lista de generos y miro las ocurrencias con el genero por parametro
		int ocurrencias1 = 0;
		for(int i = 0; i < generos1.darNumeroElementos(); i++)
		{
			if(generos1.darElemento(i).equals(genero))
			{
				ocurrencias1++;
			}
		}
		//Recorro dia 2 y miro sus generos
		ListaEnlazadaSimple<VOGeneroPelicula> generos2 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia2.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero2 = new VOGeneroPelicula();
			for(int j = 0; j<dia2.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero2.setNombre(dia1.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos1.agregarElementoFinal(genero2);
		}
		//Ahora recorro mi lista de generos y miro las ocurrencias con el genero por parametro
		int ocurrencias2 = 0;
		for(int i = 0; i < generos2.darNumeroElementos(); i++)
		{
			if(generos2.darElemento(i).equals(genero))
			{
				ocurrencias2++;
			}
		}
		//Recorro dia 3 y miro sus generos
		ListaEnlazadaSimple<VOGeneroPelicula> generos3 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia3.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero3 = new VOGeneroPelicula();
			for(int j = 0; j<dia3.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero3.setNombre(dia3.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos3.agregarElementoFinal(genero3);
		}
		//Ahora recorro mi lista de generos y miro las ocurrencias con el genero por parametro
		int ocurrencias3 = 0;
		for(int i = 0; i < generos3.darNumeroElementos(); i++)
		{
			if(generos3.darElemento(i).equals(genero))
			{
				ocurrencias3++;
			}
		}
		//Recorro dia 4 y miro sus generos
		ListaEnlazadaSimple<VOGeneroPelicula> generos4 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia4.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero4 = new VOGeneroPelicula();
			for(int j = 0; j<dia4.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero4.setNombre(dia4.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos4.agregarElementoFinal(genero4);
		}
		//Ahora recorro mi lista de generos y miro las ocurrencias con el genero por parametro
		int ocurrencias4 = 0;
		for(int i = 0; i < generos1.darNumeroElementos(); i++)
		{
			if(generos1.darElemento(i).equals(genero))
			{
				ocurrencias4++;
			}
		}
		//Recorro dia 5 y miro sus generos
		ListaEnlazadaSimple<VOGeneroPelicula> generos5 = new ListaEnlazadaSimple<VOGeneroPelicula>();
		for(int i = 0; i < dia5.darNumeroElementos(); i++)
		{
			VOGeneroPelicula genero5 = new VOGeneroPelicula();
			for(int j = 0; j<dia5.darElemento(i).getPelicula().getGeneros().length; j++)
			{
				genero5.setNombre(dia5.darElemento(i).getPelicula().getGeneros()[j]);
			}
			generos1.agregarElementoFinal(genero5);
		}
		//Ahora recorro mi lista de generos y miro las ocurrencias con el genero por parametro
		int ocurrencias5 = 0;
		for(int i = 0; i < generos5.darNumeroElementos(); i++)
		{
			if(generos5.darElemento(i).equals(genero))
			{
				ocurrencias5++;
			}
		}
		//Busco el plan con mas ocurrencias del genero pedido
		String masOcurrencias = "";
		int masOcurrente = ocurrencias5;
		ListaEncadenada<Integer> misOcurrencias = new ListaEncadenada<Integer>();
		misOcurrencias.agregarElementoFinal(ocurrencias1);
		misOcurrencias.agregarElementoFinal(ocurrencias2);
		misOcurrencias.agregarElementoFinal(ocurrencias3);
		misOcurrencias.agregarElementoFinal(ocurrencias4);
		misOcurrencias.agregarElementoFinal(ocurrencias5);
		for(int i = 0; i<misOcurrencias.size(); i++)
		{
			System.out.println(misOcurrencias.darElemento(i));
			if(masOcurrente<misOcurrencias.darElemento(i)){
				masOcurrente=misOcurrencias.darElemento(i);
				masOcurrencias=""+i;
			}
		}
		//Decido cual es la lista que debo retornar
		if(masOcurrencias.equals("0"))
		{
			lista = dia1;
		}
		else if(masOcurrencias.equals("1")){
			lista = dia2;
		}
		else if(masOcurrencias.equals("2")){
			lista = dia3;
		}
		else if(masOcurrencias.equals("3")){
			lista = dia4;
		}
		else if(masOcurrencias.equals("4")){
			lista = dia5;
		}
		return lista;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorFranquicia(VOFranquicia franquicia, int fecha, String franja) {
		ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
		VOTeatro teatroActual =null;
		long horaActual = 0;
		long dia1 = 10L;
		long dia2 = 12L;
		long tarde1 = 12L;
		long tarde2 = 18L;
		//Recorro mis peliculas
		if(franja.equals(""))
		{
			for(int i = 0; i<THPeliculas.darTamanio(); i++)
			{
				VOPelicula mejorpelicula = THPeliculas.darValor(i); 
				ListaEnlazadaSimple <VOPeliculaPlan> funcionSiguiente = mejorpelicula.darPeliculasSiguienteFuncion(fecha, horaActual);
				for(VOPeliculaPlan plan : funcionSiguiente)
				{
					if (lista.darNumeroElementos() == 0)
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							lista.agregarElementoFinal(plan);
							teatroActual = plan.getTeatro();
							horaActual = plan.getHoraFin().getTime();
							break;
						}
					}
					if (plan.getTeatro().getNombre().equals(teatroActual))
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							lista.agregarElementoFinal(plan);
							teatroActual = plan.getTeatro();
							horaActual = plan.getHoraFin().getTime();
							break;
						}
					}
					else if (plan.getHoraInicio().getTime() >= horaActual)
					{
						VOTeatro teatroSiguiente = plan.getTeatro();
						String teatro1 = teatroActual.getNombre();
						String teatro2 = teatroSiguiente.getNombre();
						double tiempoRecorrido = grafoteatros.darPesoArco(teatro1, teatro2);

						if (plan.getHoraInicio().getTime() >= (horaActual+tiempoRecorrido))
						{
							if(plan.getTeatro().getFranquicia().equals(franquicia))
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
				}
			}
		}
		else if(franja.equals("mañana"))
		{
			for(int i = 0; i<THPeliculas.darTamanio(); i++)
			{
				VOPelicula mejorpelicula = THPeliculas.darValor(i); 
				ListaEnlazadaSimple <VOPeliculaPlan> funcionSiguiente = mejorpelicula.darPeliculasSiguienteFuncion(fecha, horaActual);
				for(VOPeliculaPlan plan : funcionSiguiente)
				{
					if (lista.darNumeroElementos() == 0)
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>dia1&&plan.getHoraInicio().getTime()<dia2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					if (plan.getTeatro().getNombre().equals(teatroActual))
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>dia1&&plan.getHoraInicio().getTime()<dia2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					else if (plan.getHoraInicio().getTime() >= horaActual)
					{
						VOTeatro teatroSiguiente = plan.getTeatro();
						String actual = teatroActual.getNombre();
						String siguiente = teatroSiguiente.getNombre();
						double tiempoRecorrido = grafoteatros.darPesoArco(actual, siguiente);

						if (plan.getHoraInicio().getTime() >= (horaActual+tiempoRecorrido))
						{
							if(plan.getTeatro().getFranquicia().equals(franquicia))
							{
								if(plan.getHoraInicio().getTime()>dia1&&plan.getHoraInicio().getTime()<dia2)
								{
									lista.agregarElementoFinal(plan);
									teatroActual = plan.getTeatro();
									horaActual = plan.getHoraFin().getTime();
									break;
								}
							}
						}
					}
				}
			}
		}
		else if(franja.equals("tarde"))
		{
			for(int i = 0; i<THPeliculas.darTamanio(); i++)
			{
				VOPelicula mejorpelicula = THPeliculas.darValor(i); 
				ListaEnlazadaSimple <VOPeliculaPlan> funcionSiguiente = mejorpelicula.darPeliculasSiguienteFuncion(fecha, horaActual);
				for(VOPeliculaPlan plan : funcionSiguiente)
				{
					if (lista.darNumeroElementos() == 0)
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>tarde1&&plan.getHoraInicio().getTime()<tarde2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					if (plan.getTeatro().getNombre().equals(teatroActual))
					{
						if(plan.getTeatro().getFranquicia().equals(franquicia))
						{
							if(plan.getHoraInicio().getTime()>tarde1&&plan.getHoraInicio().getTime()<tarde2)
							{
								lista.agregarElementoFinal(plan);
								teatroActual = plan.getTeatro();
								horaActual = plan.getHoraFin().getTime();
								break;
							}
						}
					}
					else if (plan.getHoraInicio().getTime() >= horaActual)
					{
						VOTeatro teatroSiguiente = plan.getTeatro();
						String actual = teatroActual.getNombre();
						String siguiente = teatroSiguiente.getNombre();
						double tiempoRecorrido = grafoteatros.darPesoArco(actual, siguiente);

						if (plan.getHoraInicio().getTime() >= (horaActual+tiempoRecorrido))
						{
							if(plan.getTeatro().getFranquicia().equals(franquicia))
							{
								if(plan.getHoraInicio().getTime()>tarde1&&plan.getHoraInicio().getTime()<tarde2)
								{
									lista.agregarElementoFinal(plan);
									teatroActual = plan.getTeatro();
									horaActual = plan.getHoraFin().getTime();
									break;
								}
							}
						}
					}
				}
			}
		}
		for (VOPeliculaPlan peli : lista)
		{
			System.out.println(peli.getPelicula().getTitulo());
		}
		return lista;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(VOGeneroPelicula genero, int fecha) {
		ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
		VOPeliculaPlan mejorPlan = null;
		for (VOTeatro teatro : teatros){
			VOPeliculaPlan mejorInterno = teatro.getPrimeraFuncion(fecha, genero.getNombre());
			if (mejorInterno == null) continue;
			if (mejorPlan == null || mejorInterno.getHoraInicio().getTime() < mejorPlan.getHoraInicio().getTime())
				mejorPlan = mejorInterno;
		}
		lista.agregarElementoFinal(mejorPlan);

		VORecorrido recorrido = new VORecorrido(mejorPlan.getHoraFin(), 0.0, lista, mejorPlan.getTeatro());
		VORecorrido max = new VORecorrido(mejorPlan.getHoraFin(), 0.0, lista, mejorPlan.getTeatro());
		max = buscarPlan (recorrido, max, null, fecha, genero.getNombre(), null, 46, ultimaFuncion);
		return max.listaFunciones;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(VOGeneroPelicula genero, int fecha,
			VOFranquicia franquicia) {
		ListaEnlazadaSimple< VOPeliculaPlan> lista = new ListaEnlazadaSimple<VOPeliculaPlan>();
		VOPeliculaPlan mejorPlan = null;
		for (VOTeatro teatro : teatros){
			if (!teatro.getFranquicia().getNombre().equalsIgnoreCase(franquicia.getNombre()))
				continue;
			VOPeliculaPlan mejorInterno = teatro.getPrimeraFuncion(fecha, genero.getNombre());
			if (mejorInterno == null) continue;
			if (mejorPlan == null || mejorInterno.getHoraInicio().getTime() < mejorPlan.getHoraInicio().getTime())
				mejorPlan = mejorInterno;
		}
		lista.agregarElementoFinal(mejorPlan);

		VORecorrido recorrido = new VORecorrido(mejorPlan.getHoraFin(), 0.0, lista, mejorPlan.getTeatro());
		VORecorrido max = new VORecorrido(mejorPlan.getHoraFin(), 0.0, lista, mejorPlan.getTeatro());
		max = buscarPlan (recorrido, max, null, fecha, genero.getNombre(), franquicia, 46, ultimaFuncion);
		return max.listaFunciones;
	}

	@Override
	public ILista<IEdge<VOTeatro>> generarMapa() {
		ListaEncadenada<IEdge<VOTeatro>> retorno = new ListaEncadenada<>();
		Iterable <Edge<VOTeatro>> iterable = grafo.prim();
		if (iterable == null)
			return null;
		for (Edge<VOTeatro> edge : iterable)
		{
			retorno.agregarElementoFinal(edge);
		}
		return (ILista<IEdge<VOTeatro>>) retorno;
	}

	@Override
	public ILista<ILista<VOTeatro>> rutasPosible(VOTeatro origen1, int n) {
		EncadenamientoSeparadoTH<String, NodoCamino<String>> rta;
		ListaEnlazadaSimple rta1 = new ListaEnlazadaSimple();
		String origen = origen1.getNombre();
		rta = grafoteatros.DFS(origen);
		for (int i = 0; i < rta.darTamanio(); i++) 
		{
			int visit = n;
			ListaEncadenada<VOTeatro> minirta = new ListaEncadenada<>();
			VOTeatro actual = (VOTeatro) ((NodoCamino)rta.darValor(""+i)).darOrigen();
			String actual1 = actual.getNombre();
			rta = grafoteatros.DFS(actual1);
			for (int j = 0; j < rta.darCapacidad(); j++) {
				if(visit>=0){
					minirta.agregarElementoFinal((VOTeatro) ((NodoCamino)rta.darValor(""+j)).darLlegada());
				}
				visit--;
			}
			rta1.agregarElementoFinal(minirta);
		}
		return rta1;
	}
	
	private VORecorrido buscarPlan(VORecorrido recorrido, VORecorrido max, VOUsuario usuario, int fecha, String genero, VOFranquicia franquicia, int numArcos, Time tiempoMax) {
		ListaEnlazadaSimple<Edge> lista = grafoteatros.adj(recorrido.teatro.getNombre(), numArcos);
		for (Edge <VOTeatro> edge : lista){
			VOTeatro llegada = edge.other(recorrido.teatro);
			if (franquicia != null && !llegada.getFranquicia().getNombre().equalsIgnoreCase(franquicia.getNombre()))
				continue;
			Time tiempo = new Time(recorrido.tiempoActual.getTime() + ((int)(darMultiplicadorFranja(recorrido.tiempoActual)*edge.weight()*60000)));
			if (tiempo.getTime() > tiempoMax.getTime()){
				if (max.compareTo(recorrido) <0)
					max = recorrido;
				continue;
			}
			VOPeliculaPlan peliPlan = buscarPlanRecomendado (usuario, llegada, fecha, tiempo, recorrido, genero);
			if (peliPlan == null){
				if (max.compareTo(recorrido) <0)
					max = recorrido;
				continue;
			}
			VORecorrido nuevoRecorrido = null;
			if (usuario != null)
				nuevoRecorrido = recorrido.darNuevoRecorrido(usuario.darRecomendacion(peliPlan.getPelicula()), peliPlan, llegada);
			else
				nuevoRecorrido = recorrido.darNuevoRecorrido(0.0, peliPlan, llegada);
			if (nuevoRecorrido.tiempoActual.getTime() > tiempoMax.getTime()){
				if (max.compareTo(nuevoRecorrido) <0)
					max = nuevoRecorrido;
				continue;
			}
			else
				max = buscarPlan(nuevoRecorrido, max, usuario, fecha, genero, franquicia, numArcos, tiempoMax);
		}
		return max;
	}
	
	public double darMultiplicadorFranja (Time tiempo){
		long time = tiempo.getTime();
		if (time <= franjasHorarias[0][1].getTime())
			return 1.0;
		else if (time <= franjasHorarias[1][1].getTime())
			return 1.15;
		else
			return 1.2;
	}
	
	private VOPeliculaPlan buscarPlanRecomendado(VOUsuario usuario, VOTeatro llegada, int dia, Time tiempo, VORecorrido recorrido, String genero) {
		VOPeliculaPlan recomendado = null;
		for (VOPeliculaPlan peliPlan : llegada.getFunciones(tiempo, dia)){
			if (recomendado!= null && peliPlan.getHoraInicio().getTime() - tiempo.getTime() > 3600*1000)
				break;
			if (usuario == null){
				if ((genero == null || peliPlan.getPelicula().esGenero(genero)) && !recorrido.tienePelicula(peliPlan.getPelicula()))
					return peliPlan;
				else
					continue;
			}
			if ((genero == null || peliPlan.getPelicula().esGenero(genero)) && !recorrido.tienePelicula(peliPlan.getPelicula()))
				if (recomendado == null || (usuario.compararPredicciones(recomendado.getPelicula(), peliPlan.getPelicula())<0))
					recomendado = peliPlan;
		}
		return recomendado;
	}
}
