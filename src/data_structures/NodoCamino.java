package data_structures;

public class NodoCamino<K> implements Comparable <NodoCamino<K>>{
	private K from;
	private K to;
	private boolean mark;
	private double peso;
	private char ordenL;
	private K idFinal;
	private K idAdy;
	public double weight;
	public int length;
	public NodoCamino(K Ffrom,K Tto)  {
		// TODO Auto-generated constructor stub
		from = Ffrom;
		to=Tto;
		peso=0;
		ordenL='0';
	}
	public NodoCamino(K Ffrom,K Tto,char pOrden,double pPeso) {
		// TODO Auto-generated constructor stub
		from = Ffrom;
		to=Tto;
		ordenL=pOrden;
		peso=pPeso;
		this.idFinal = idFinal;
		this.idAdy = idAdy;
		this.weight = weight;
		this.length = length;
		
	}
	
	public K either() {
		return idFinal;
	}

	public K other(K vertice) {
		return (vertice.equals(idFinal))? idAdy:idFinal;
	}
	
	public String toString (){
		return  idAdy + "<->" + idFinal + " - W: " + weight + " - L: " + length; 
	}
	
	public K darOrigen(){
		return from;
		
	}
	public K darLlegada(){
		return to;
	}
	public double darPeso(){
		return peso;
	}
	@Override
	public int compareTo(NodoCamino<K> arg0) {
		if (weight == arg0.weight)
			return 0;
		return (weight <= arg0.weight)? -1:1;
	}
}