package VOS;

import java.sql.Time;

import data_structures.ListaEnlazadaSimple;

public class VORecorrido implements Comparable <VORecorrido>{
	public Time tiempoActual;
	public ListaEnlazadaSimple<VOPeliculaPlan> listaFunciones;
	public double sumRecomendacion;
	public VOTeatro teatro;

	public VORecorrido (Time tiempoActual, double sumRecomendacion, ListaEnlazadaSimple<VOPeliculaPlan> listaFunciones, VOTeatro teatro){
		this.tiempoActual = tiempoActual;
		this.sumRecomendacion = sumRecomendacion;
		this.listaFunciones = listaFunciones;
		this.teatro = teatro;
	}
	public VORecorrido (Time tiempoActual){
		this.tiempoActual = tiempoActual;
		this.listaFunciones = new ListaEnlazadaSimple<>();
	}
	public VORecorrido (){
		this.listaFunciones = new ListaEnlazadaSimple<>();
	}
	public int numFunciones (){
		return listaFunciones.darNumeroElementos();
	}
	public void agregarFuncion (VOPeliculaPlan peliPlan){
		listaFunciones.agregarElementoFinal(peliPlan);
	}
	private ListaEnlazadaSimple<VOPeliculaPlan> darListaFunciones (VOPeliculaPlan peliPlan){
		ListaEnlazadaSimple<VOPeliculaPlan> lista = listaFunciones.darPrimerosElementos(listaFunciones.darNumeroElementos());
		lista.agregarElementoFinal(peliPlan);
		return lista;
	}
	public VORecorrido darNuevoRecorrido(double recomendacion, VOPeliculaPlan peliPlan, VOTeatro teatroP){
		double recom = 0.0;
		if (Double.isNaN(sumRecomendacion))
			recom = recomendacion;
		else
			recom = sumRecomendacion + recomendacion;
		return new VORecorrido (peliPlan.getHoraFin(), recom, darListaFunciones(peliPlan), teatroP);
	}

	@Override
	public int compareTo(VORecorrido o) {
		if (numFunciones() == o.numFunciones()){
			if (sumRecomendacion == o.sumRecomendacion){
				if (tiempoActual.getTime() == o.tiempoActual.getTime())
					return 0;
				return (tiempoActual.getTime() < o.tiempoActual.getTime())? 1:-1;
			}
			return (sumRecomendacion < o.sumRecomendacion)? -1:1;
		}
		return (numFunciones() < o.numFunciones()) ? -1:1;
	}

	public String toString(){
		return "Pelis:" + numFunciones() + " - Pred:" + sumRecomendacion + " - " + tiempoActual;
	}
	public boolean tienePelicula(VOPelicula peli){
		for(VOPeliculaPlan peliPlan: listaFunciones)
			if (peliPlan.getPelicula().equals(peli))
				return true;
		return false;
	}
}

